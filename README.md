The Image Flipper Formatter module enhances your Drupal website's user experience by providing a dynamic way to display images. With this module, you can effortlessly change the field formatter for your images, enabling them to flip when hovered over by the mouse. This engaging feature adds a touch of interactivity to your website, captivating your visitors and making their browsing experience more enjoyable.

## Features:
- Custom Field Formatter: The module allows you to easily switch between field formatters for your images, giving you the flexibility to choose the presentation style that best suits your website's design.
- Interactive Image Flipping: On the front end, images will flip when hovered over by the mouse, providing an interactive element that enhances user engagement and interaction with your content.

## Post-Installation Instructions:
Navigate to the content type page within your Drupal administration interface.
Select the specific content type where you want to apply the Image Flipper Formatter.
Click on "Manage Display" for the chosen content type.
Locate the image field for which you want to enable the Image Flipper Formatter.
In the display settings for the image field, select "Image Flipper Formatter" from the available options.
After making your selection, click "Submit" to save the changes.
View the updated page on the front end to see the Image Flipper Formatter in action, where images will now flip on mouse hover, adding an interactive touch to your content presentation.



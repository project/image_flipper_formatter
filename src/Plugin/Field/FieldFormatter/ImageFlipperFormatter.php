<?php

namespace Drupal\image_flipper_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'image_flipper_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "image_flipper_formatter",
 *   label = @Translation("Image Flipper Formatter"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageFlipperFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      // Customize the rendering of the image with flip effect here.
      $elements[$delta] = [
        '#theme' => 'image_flipper_formatter',
        '#url' => $item->entity->createFileUrl(),
        '#alt' => $item->alt,
      ];
    }

    // Attach library.
    $elements['#attached']['library'][] = 'image_flipper_formatter/image_flipper_formatter';
    return $elements;
  }

}
